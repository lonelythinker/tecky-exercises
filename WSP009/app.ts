//import pg from 'pg';
import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();
//import xlsx from 'xlsx';


export const client = new Client({
    user:process.env.DB_USERNAME,    //wsp009
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    port:5432
});

// interface user{       //create一個type叫user, 比usersJSON用
//     username:string
//     password:string
//     level:string
// }

// interface file{
//     name:string
//     content:string
//     isFile:boolean
// }

// interface category{
//     name:string
// }

async function main(){
    //撥號 to PostregSQL
    await client.connect()
    //const fileData = xlsx.readFile('./WSP009-exercise.xlsx')
    //console.log(fileData) //excel檔所有sheet的總和

    //const usersJSON:user[] = xlsx.utils.sheet_to_json(fileData.Sheets['user'])  // (fileData.Sheets['user']),將excel中user呢張sheet既資料讀出，係睇唔明既format
                                                                                   // xlsx.utils.sheet_to_json, 再將果d睇唔明既格式轉做json. 以first row為key, 對應之下的rows是values
    //console.log(usersJSON)
    // for(let user of usersJSON){    
    //     //await client.query(/*sql*/ `insert into "user" (username,password,level) values ('${user.username}','${user.password}','${user.level}')`);  呢種寫法有可能被SQL Injection, 千奇唔好咁寫!!
    //     await client.query(/*sql*/ `insert into "user" (username,password,level) values ($1,$2,$3)`, [user.username,user.password,user.level])    // 1. 點解咁寫會分別配對返$1,2,3? 2. 點解咁寫會escape到SQL?
    // }

    // const filesJSON:file[] = xlsx.utils.sheet_to_json(fileData.Sheets['file'])

    // for(let file of filesJSON){
    //     await client.query(/*sql*/ `insert into files (name,content,is_File) values ($1,$2,$3)`, [file.name,file.content,file.isFile])    
    // }
    const res = await client.query(`select * from students;`)
    console.log(res.rows)
    // const catsJSON:category[] = xlsx.utils.sheet_to_json(fileData.Sheets['category'])
    
    // for(let category of catsJSON){
    //     await client.query(/*sql*/ `insert into categories (name) values ($1)`, [category.name])
    // }    

    //const result = await client.query(/*sql*/ `select * from files`)   // 1. 讀sql內既data睇睇 2./*sql*/ <--- 可加返syntax highlighting  3.如果唔加await, 未等個file讀完就收左線
    //console.log(result.rows)    

    //收線
    await client.end()
}
main()

















// if(typeof require !== 'undefined'){
//     let XLSX = require('xlsx')
//     const workbook = XLSX.readFile('WSP009-exercise.xlsx')
//     XLSX.utils.sheet_to_json(workbook)
//     console.log(workbook)
// } 

// if(typeof require !== 'undefined'){
//     const client = new pg.Client()
// await client.connect()

// const res = await client.query('SELECT $1::text as message', ['Hello world!'])
// console.log(res.rows[0].message) // Hello world!
// await client.end()
// }
