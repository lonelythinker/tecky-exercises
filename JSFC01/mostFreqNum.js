const markSix ="3 5 30 31 43 48 2 11 13 45 46 49 11 14 21 28 37 44 18 29 32 33 36 40 2 20 24 30 32 46 5 17 35 37 42 49 1 24 25 27 31 37 15 17 29 30 34 37 5 10 18 20 28 33 1 22 25 27 31 36".split(" ")

const checkOccurance = {}

let x = 0;
while (x < markSix.length) {
    if (checkOccurance[markSix[x]] == null) {
        checkOccurance[markSix[x]] = 0
    }
    checkOccurance[markSix[x]] = checkOccurance[markSix[x]] + 1;
    x = x + 1;
}

console.log(checkOccurance)

let maxNum = 0;
let maxOccurance = 0;
for (const key in checkOccurance) {            
    if (checkOccurance[key] > maxOccurance) {   
        maxNum = key;                          
        maxOccurance = checkOccurance[key];    
    }
}
console.log(maxNum + " is the most frequent number and it appeared " + maxOccurance + " times.")