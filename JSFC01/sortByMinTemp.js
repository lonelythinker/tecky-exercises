const tempforecast= [
    {"date": "1 Jan", "minTemperature": 11, "maxTemperature": 18},
    {"date": "2 Jan", "minTemperature": 10, "maxTemperature": 17},
    {"date": "3 Jan", "minTemperature": 12, "maxTemperature": 17},
    {"date": "4 Jan", "minTemperature": 14, "maxTemperature": 19},
    {"date": "5 Jan", "minTemperature": 16, "maxTemperature": 21},
    {"date": "6 Jan", "minTemperature": 17, "maxTemperature": 21},
    {"date": "7 Jan", "minTemperature": 18, "maxTemperature": 22},
]

console.log(tempforecast.sort(function(minA, minB) {
    if (minA.minTemperature > minB.minTemperature) {
        return 1
    } else if (minA.minTemperature < minB.minTemperature) {
        return -1
    } else {
        return 0
    }
  }))