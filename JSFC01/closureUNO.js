let alex = createPlayer();
let gordon = createPlayer();

alex.win([1, 2, 9, 8])
alex.win([3])
alex.win([0, 0, 0, 1, 3])
gordon.win([4, 4, 9])
gordon.win([2, 2, 4, 5, 6, 6, 7, 3])

function createPlayer() {
    let score = 0;
        return {
            win: function (cards) {                          
                for (const points of cards) {
                    score = score + points;
                }         
            },  
            result: function (){
            return score;           
            }
        }
}

console.log("Alex's score : " + alex.result())
console.log("Gordon's score : " + gordon.result())

//console.log(`Alex's score: ${alex.result()}`);
//console.log(`Gordon's score: ${gordon.result()}`);
