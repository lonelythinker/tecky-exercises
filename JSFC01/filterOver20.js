const tempforecast= [
    {"date": "1 Jan", "minTemperature": 11, "maxTemperature": 18},
    {"date": "2 Jan", "minTemperature": 10, "maxTemperature": 17},
    {"date": "3 Jan", "minTemperature": 12, "maxTemperature": 17},
    {"date": "4 Jan", "minTemperature": 14, "maxTemperature": 19},
    {"date": "5 Jan", "minTemperature": 16, "maxTemperature": 21},
    {"date": "6 Jan", "minTemperature": 17, "maxTemperature": 21},
    {"date": "7 Jan", "minTemperature": 18, "maxTemperature": 22},
]

console.log(tempforecast.filter(function(abc){        
    if (abc.maxTemperature > 20){
        return true;
    } else {
        return false;
    }
}).map(function(def){    //已filter出符合條件的objects, 因此可以基於object, 再連續技下去.map()
    return def.date
}))