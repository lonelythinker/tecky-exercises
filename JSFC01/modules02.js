const readLineSync = require("readLine-Sync")
const add = require("date-fns/add")
const parseISO = require("date-fns/parseISO")
const format = require("date-fns/format")

//Using readline-sync and date-fns package, calculate the n-day for the celebration for an event. For example:
//Enter the key date: 2020-02-02
let date = parseISO('2020-04-05')
let formatedDate = format(date, 'dd-MM-yyyy')

console.log(date)
console.log(formatedDate)
console.log(format(add(date, {days: 30}), "MMMM"))     //個指定format"dd-MM-yyyy", 要係add個()之內輸入!!

//console.log(format(add(date, {days: 10})), "dd-MM-yyyy")  //此行為錯誤示範

//console.log(format((add(date, {days: 1}))))

/*
while(true) { 
const strDate = readLineSync.question("What is your key date? ");
const date = parseISO(strDate)

console.log("100-day celebration: " + format(add(date, {days: 100}),  "yyyy-MM-dd"  ))
console.log("3-month celebration: " + format(add(date, {months: 3}),  "yy-MM-dd"  ))
console.log("10000000-second celebration: "+ format(add(date, {seconds: 10000000}), "yyyy-MM-dd"))
}
*/
// 100-day celebration: 2020-05-12 
// 100-month celebration: 2028-06-02 
// 10000000-second celebration: 2020-05-27 

// Enter the key date: 2047-7-1
// Repeat the above...

