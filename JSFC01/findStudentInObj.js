const tallStudents = []
const students = [
    {"name": "Peter", "height": 175, "hobbies": ["football", "basketball"]},
    {"name": "John", "height": 180, "hobbies": ["basketball", "sleeping"]},
    {"name": "Mary", "height": 160, "hobbies": ["eating", "sleeping", "reading"]},
    {"name": "Betty", "height": 170, "hobbies": ["reading"]},
]

let x = 0
while (x < students.length) {
    if (students[x].height > 170) {
        tallStudents.push(students[x]["name"] + ": " + students[x]["height"])
    }
    x = x + 1;
} 

console.log(tallStudents)