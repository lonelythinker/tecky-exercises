document.querySelector('#addmemo')
            .addEventListener('submit',async (event)=>{
                event.preventDefault(); // No need to reload the page
                const form = event.target;
                const formData = new FormData();
                formData.append('content',form.content.value);
                formData.append('image',form.image.files[0])
                const res = await fetch('/add-memo',{
                    method:"POST",
                    body:formData
                }); // your_hostname/add-memo
                const result = await res.json();
                document.querySelector('#add-memo-message')
                        .classList.remove('d-none');

                loadMemos();
            })

async function loadMemos(){
    // 1. Load Data
    const res = await fetch('/memos');
    const memos = await res.json();
    console.log(memos);

    // 2. Loop Data
    const memoContainer = document.querySelector('.memo-container');
    memoContainer.innerHTML = "";


    for(let memo of memos){
            // 3. 逐個DATA  塞返落去
        memoContainer.innerHTML += `
            <div class="memo" contenteditable>
                ${memo.content}
                ${
                    memo.image ? `
                        <img src="/uploads/${memo.image}" 
                        alt=${memo.content}
                        class="img-fluid"
                        />
                    `:""
                }
                <div class="icon bottom-right edit-memo" data-id="${memo.id}">
                    <i class="fas fa-edit"></i>
                </div>
                <div class="icon top-right delete-memo" data-id="${memo.id}">
                    <i class="fas fa-trash-alt"></i>
                </div>
            </div>
        `
    }

    memoContainer.onclick = async function(event){
        if(event.target.matches('.delete-memo')){
            const id = event.target.getAttribute('data-id');
            const res = await fetch(`/memos/${id}`,{
                method:"DELETE"
            });
            const result = await res.json();
            loadMemos();
        }else if(event.target.matches('.edit-memo')){
            const id = event.target.getAttribute('data-id');
            const parentMemo = event.target.closest('.memo');
            const res = await fetch(`/memos/${id}`,{
                method:"PUT",
                headers:{
                    "Content-Type":"application/json"
                },
                body:JSON.stringify({
                    content: parentMemo.innerText
                })
            });
            const result = await res.json();
        }
    }
}

loadMemos();