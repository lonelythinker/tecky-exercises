import express,{Request,Response,NextFunction} from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import path from 'path';
import jsonfile from 'jsonfile';
import multer from 'multer';

// Server side code    X accessible by end-users
const app = express();

app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
const upload = multer({storage})

app.get('/',function(req:Request,res:Response,next:NextFunction){
    
    if(req.session){
        if(req.session.counter){
            req.session.counter += 1;
        }else{
            req.session.counter = 1;
        }
        console.log(`Counter is ${req.session.counter}`);
    }
    next();
})


app.post('/add-memo',upload.single('image'),async function(req,res){
    // const {content} = req.body;
    const content = req.body.content;
    const memos = await jsonfile.readFile('./memos.json');
    let maxMemoId:number  = 0;
    for(let memo of memos){
        if(memo.id > maxMemoId ){
            maxMemoId = memo.id;
        }
    }
    memos.push({
        id: maxMemoId +1,// 保證唔會同其他撞
        content,
        image: req.file.filename
    });
    await jsonfile.writeFile('./memos.json',memos,{spaces:4});
    res.json({success:true});
})


app.put('/memos/:id',async (req,res)=>{
    const memoId = parseInt(req.params.id);
    if(isNaN(memoId)){
        res.status(400).json({msg:"id is not a number!"});
        return;
    }
    const memos = await jsonfile.readFile('./memos.json');

    const {content} = req.body;

    for(let memo of memos){
        if(memo.id === memoId){
            memo.content = content;
            break;
        }
    }

    await jsonfile.writeFile('./memos.json',memos,{spaces:4});
    res.json({success:true});
});

app.delete('/memos/:id',async (req,res)=>{
    const memoId = parseInt(req.params.id);
    if(isNaN(memoId)){
        res.status(400).json({msg:"id is not a number!"});
        return;
    }
    const memos = await jsonfile.readFile('./memos.json');

    const deletedMemos = memos.filter((memo:any)=>memo.id !== memoId);
    await jsonfile.writeFile('./memos.json',deletedMemos,{spaces:4});
    res.json({success:true});
});

app.get('/memos',async function(req,res){
    const memos = await jsonfile.readFile('./memos.json');
    res.json(memos);
})


interface User{
    username:string
    password:string
}

const users:User[] = [
    {
        username:"gordon@tecky.io",
        password:"tecky"
    },
    {
        username:"alex@tecky.io",
        password:"tecky"
    },
]


app.post('/login',function(req,res){
    const {username,password} = req.body;
    for(let user of users){
        if(user.username === username && user.password === password){
            // login 成功
            if(req.session){
                req.session.user = user; // 係個session 度mark 咗你係成功login 咗
            }
            res.redirect('/admin.html');
            return;
        }
    }
    // login 失敗
    res.status(401).redirect("/");
});


app.use(function(req,res,next){
    console.log(`[${new Date().toISOString()}] Request ${req.path}`);
    next();
});

const isLoggedIn = function(req:Request,res:Response,next:NextFunction){
    console.log(req.session);
    if(req.session){
        if(req.session.user){
            next();
            return;
        }
    }

    res.redirect('/');
}


app.use(express.static('public'));
app.use(isLoggedIn,express.static('protected'));

app.use(function(req,res){
    res.sendFile(path.join(__dirname,'public/404.html'));
})


app.listen(8080,function(){
    console.log("Express listening at http://localhost:8080");
})