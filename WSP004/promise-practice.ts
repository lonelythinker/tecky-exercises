import fs from 'fs'

//const promisify = require('util').promisify;

//const dijkstraQuote1 = "Computer science is no more about computers than astronomy is about telescopes.\n";
//const dijkstraQuote2 = "Simplicity is prerequisite for reliability.\n";

//     try{
//         fs.writeFileSync('quotes-dijkstra.txt',dijkstraQuote1,{flag:'w'})
//         fs.writeFileSync('quotes-dijkstra.txt',dijkstraQuote2,{flag:'a+'})
//         fs.writeFileSync('quotes-dijkstra.txt',"Some other text\n",{flag:'a+'});
//         fs.writeFileSync('quotes-dijkstra.txt',"More some other stuff", {flag: 'a+'})

//     }catch(err){
//         console.log(err);
//     } 


// Promisify the fsReadFile
// const fsReadFilePromise = new Promise<Buffer>(function (resolve, reject) {
//     fs.readFile('test2.txt', function (err: Error, Buffer) {
//         if (err) {
//             reject(err);                            //暗渡陳倉? so magical! How does it really work?

//         } else {
//             resolve(Buffer);                        //暗渡陳倉? so magical! How does it really work?
//         }
//     });
// });

// 一個包住左透過Promise constructor而Promisfy左既fs.readFile既function可供呼叫︰

// function fsReadFilePromise(file:string) {                        //line 31行個file係line 33果行file食既para
//     return new Promise<Buffer>(function (resolve, fail) {    //Promise係張order紙, 讀file入黎個type by default叫Buffer, 一d Binary既東西
//         fs.readFile(file, function (Opps, yeah) {    
//             if (Opps) {
//                 fail(Opps);
//             } else {
//                 resolve(yeah); //呢個yeah係一個「Buffer」 class with Raw data about file,之後then handler就會接收呢個data
//                 console.log("Step Inside Promise Constructor")
//             }
//         });
//     });
// }
// Promisfy fs.stat() with magical Promise-constructor
function fsStatPromise(file:string){
    return new Promise<fs.Stats>(function(resolve,reject){
        fs.stat(file,function(err,stats){
            if(err){
                reject(err.message)
            } else {
                if(stats.isFile() === true){
                    resolve(stats)
                }
            }
        })
    })
}
// 呼叫已Promise based的fs.readFile, 可以係呢個callback function之外使用，令一啲因Nodejs本質上Async既動作可以sequential,逐步逐步行,做完一個先一個。
// fsReadFilePromise('test1.txt')
//     .then(function(txt1) {             //呢個txt1係line 39個resolve營救出黎既 yeah
//         console.log(txt1.toString())   //如果冇.toString(), data係呢舊「Buffer 54 68 69 73 20 69 73 20 74 65 73 74 20 32」野
//         console.log("Step 1")           
//         return fsReadFilePromise('test2.txt')    //magical thing happens again, 'test2.txt'可以pass比下一個.then handler!!
//     })
//     .then(function(txt2) {
//         console.log(txt2.toString())
//         console.log("Step 2")
//         return fsReadFilePromise('test3.txt')
//     })
//     .then(function(txt3) {
//         console.log(txt3.toString())
//         console.log("Step 3")
//         return 'This is TEST 4'       //不一定要return promise, return其他野都可以，亦可見到因為不是promise, Step Inside Promise Constructor那一步沒有出現
//     })
//     .then(function(txt4) {                          //承上，故line 75的txt type是string而非Buffer  
//         console.log(txt4.toString())
//         console.log("Step 4")
//         return fsReadFilePromise('test5.txt')               //下面已經冇.then handler接, 即使真係有test5.txt都冇反應了
//     })
//     .catch(function(err) {                       
//         console.log(err.message)
//     })
//------------------------------------------------------------
//透過Promise.all可以一次同時將多個Promise排序，全部做完先一齊console.log, array內的次序決定執行先後
//Promise.all()內可有多於一種的promise, fsreadFile + fsStat一齊.all都可以
// Promise.all([fsReadFilePromise('test1.txt'),fsReadFilePromise('test2.txt'),fsReadFilePromise('test3.txt'),fsReadFilePromise('test4.txt'),fsStatPromise('test7.txt'), fsReadFilePromise('test6.txt')])
//     .then(function(data){
//         console.log("This is Promise ALL")
//         const [test1,test2,test3,test4,test5,test6] = data;
//         console.log("By Promise.all: " + test1)
//         console.log("By Promise.all: " + test2)
//         console.log("By Promise.all: " + test3)
//         console.log("By Promise.all: " + test4)
//         console.log("By Promise.all: " + test5)
//         console.log("By Promise.all: " + test6)
//     })

fsStatPromise('test1.txt')
    .then(function(data){     //如果function()入面冇入data, then handler就冇野處理，做乜就睇line 101有冇野做
        if(data){
            console.log('true')
        }
    })