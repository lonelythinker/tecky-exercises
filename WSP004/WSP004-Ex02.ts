//import util from 'util'
import * as readline from 'readline';
//import * as events from 'events';
// const SetTimeoutPromise = util.promisify(setTimeout)

// async function runAfter1(){
//     console.log("First Line")
//     SetTimeoutPromise(3000)  //實驗1，如果唔寫await, 咁就會即刻行console.log果兩行先，再行setTimeoutPromise,做唔到順序效果
//     console.log("Second Line")
// }
// runAfter1()

// async function runAfter2(){
//     console.log("First Line")
//     await setTimeout(function(){},3000)   //實驗2，如果冇做promisfy, 都係會即刻行console.log果兩行先，再行setTimeout,亦做唔到順序效果
//     console.log("Second Line")
// }
// runAfter2()

// async function runAfter3(){
//     console.log("First Line")
//     await SetTimeoutPromise(3000)       //實驗3，做齊promisfy setTimeout及寫await, 咁就可以做到預期順序。
//     console.log("Second Line")
// }
// runAfter3()

//不用util, 用promise constructor手動寫 :
// function SetTimeoutPromiseManually(ms:number){
//     return new Promise(function(resolve,reject){
//         setTimeout(function(){
//             resolve()                    //resolve()即係運行完成，話比event loop知呢度做完，返黎睇睇。
//         },ms)
//     })
// }

// async function runAfter4(){
//     console.log("Start!")
//     await SetTimeoutPromiseManually(1000)
//     await SetTimeoutPromiseManually(2000)
//     await SetTimeoutPromiseManually(3000)
// }
// runAfter4()


const readLineInterface = readline.createInterface({                
    input: process.stdin,
    output: process.stdout
})

function readLineInterfacePromise(question:string){        
    return new Promise(function(resolve, reject){       
        readLineInterface.question(question, function(answer){
            resolve(answer)
            readLineInterface.close()
        })
    })
}

async function askQuestion(question:string) {
    await readLineInterfacePromise(question)
        .then(function(answer){
            console.log(answer  + "-driven")
        })
}


askQuestion("What's your purpose of life? ")
askQuestion("What do you wanna eat? ")
askQuestion("What's your favour color? ")
