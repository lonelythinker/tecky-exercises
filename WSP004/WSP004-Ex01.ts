import * as fs from 'fs'
import * as path from 'path'
//import util from 'util'
//import { unwatchFile } from 'fs'
//import { resolve } from 'dns'
/* Callback Version */
// function getJSF(folderPath: string) {
//     fs.readdir(folderPath, function (err, files) {         //效果等於打ls
//         if (err) {
//             console.log(err.message);
//         } else {
//             for (const jsFile of files) {
//                 fs.stat(path.join(folderPath, jsFile), function (err, hahaha) {     //hahaha原名「stats」是一個object裝住關於file的資訊
//                     if (err) {      //如果line 10就咁jsFile, 冇比返folderPath同用path.join()拼返埋個完整路徑，因為個function行既「基準點」係WSP003, 去讀WSP001既files會話WSP003冇呢d file而炒。                            
//                         console.log(err.message)
//                     } else {
//                         if (hahaha.isFile() && jsFile.endsWith(".js")) {   // 先透過line 11 fs.stat得出stats,再由stats.isFile()去判斷它是否一個file
//                             console.log(path.join(path.resolve(folderPath, jsFile)))      //個 , 係keypoint!
//                         } else if (hahaha.isDirectory()) {              // 如果hahaha係directory...
//                             getJSF(path.join(folderPath, jsFile))     // 咁就自己call自己!! 重做上面既步驟!! 
//                         }
//                     }
//                 })
//             }
//         }
//     })
// }
// getJSF('.')
// getJSF('../WSP001')

/*
Incorrect Promify Version
function readJSFiles(folderPath:string){

    new Promise(function(resolve,reject){
        fs.readdir(folderPath, function(err, files){
            if(err){
                reject(err.message)
            }resolve(files)
            
            for(let file of files){
                new Promise(function(resolve,reject){
                    fs.stat(path.join(folderPath, file),function(err,stats){     //唔係好明點解要path.join(folderPath, file)
                        if(err){
                            reject(err.message)
                        } 
                        if(stats.isFile() && file.endsWith('js')){
                                resolve(stats)
                                console.log(path.join(path.resolve(folderPath, file)))
                        } else if (stats.isDirectory()){
                            readJSFiles(path.join(folderPath, file))             //唔係好明點解要path.join(folderPath, file)
                        }
                    })
                })
            }
        })
    })  
}
readJSFiles('.')
*/

function readdirPromise(folderPath:string){
    return new Promise<string[]>(function(resolve,reject){     /* Promise<>, 意即Promise最後會比返個咩type既資料出黎，呢度係string[],一個array裝住多個string */
        fs.readdir(folderPath, function(err, files){
            if(err){
                reject(err.message)
            }resolve(files)
        })
    })
}

//const readdirPromise = util.promisify(fs.readdir)

readdirPromise('./')
    .then(function(data){       //invoke readdirPromise()後要以.then handler相連，以接收resolve營救出來的資料
        console.log(data)       //但唔明點解.then(console.log)個效果同呢度一樣
    })      
    
function statPromise(filePath:string){
    return new Promise<fs.Stats>(function(resolve,reject){     
        fs.stat(filePath, function(err, stats){     //其實打hover落stats出個type落Promise<>內面咪一定岩?
            if(err){
                reject(err.message)
            }resolve(stats)
        })
    })
}

statPromise('./index.js')
    .then(function(data){
        console.log(data)
    })

export async function readJSFiles (folderPath:string){

    let files = await readdirPromise(folderPath)    //用await確保fs.readdir()讀晒指定folder內既files先, 再將結果(file name list)儲存係files呢個variable度
    for(const file of files){
        const stats = await statPromise(path.join(folderPath, file))  //path.join(folderPath, file)攞返個完整路徑
        if(stats.isFile() && file.endsWith('js')){      
            console.log(path.join(path.resolve(folderPath, file)))
        }else if (stats.isDirectory()){
            readJSFiles(path.resolve(folderPath, file))    //呢度個para係指當時check到係directory既果個file?
        }
    }
}
readJSFiles('./')

console.log(path.resolve('./','xyz.exe'))  //path.resolve('.')會自動產出所在位置的絕對路徑。加 , 分隔後的檔案名也可連在一起。 