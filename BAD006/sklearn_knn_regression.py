from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.neighbors import KNeighborsRegressor
X_train,X_test,y_train,y_test= train_test_split(boston_df,target_df,test_size=0.2,random_state=np.random.randint(10))
# Create Regressor
neigh = KNeighborsRegressor(n_neighbors=20)
neigh.fit(X_train,y_train)
print(neigh.score(X_test,y_test))