import math
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

class KNNClassifier:

    def __init__(self, X, y, n_neighbors=3):
        self.n_neighbors = n_neighbors
        self.__X = X
        self.__y = y

    def euclidean_dist(self, d1, d2):
        """calculate the euclidean distance here"""
        sum = 0
        for i in range(len(d1)): # # len(d1) = 4 (number of feature)
            sum += (d1[i] - d2[i]) ** 2
        return math.sqrt(sum)

    def predict(self, X): # X: [[], [], []] testing dataset
        """accept multiple inputs here and predict one by one with predict_single()"""
        results = []
        # for (let i = 0; i < X.length; i++) {}
        # for (const x of X) {}
        for x in X: # x = [x0, x1, x2, x3]
            result = self.predict_single(x)
            results.append(result)
        return results

    def predict_single(self, input): # input: [x0, x1, x2, x3], length = 4
        """predict single input here"""
        AK = [] # max length = self.k
        for idx, value in enumerate(self.__X):
            # print(idx, value)
            dist = self.euclidean_dist(input, value)
            AK.append({ 'idx': idx, 'distance': dist })
            if len(AK) > self.n_neighbors:
                AK = sorted(AK, key=lambda d: d['distance'])
                AK.pop()
        # print(AK)
        label_map = {}
        for d in AK:
            label = self.__y[d['idx']]
            if label in label_map:
                label_map[label] += 1
            else:
                label_map[label] = 1
        # print(label_map)
        # test_label_map = {}
        # for key in label_map:
        #     new_label = ['Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica'][key]
        #     test_label_map[new_label] = label_map[label]
        # print(test_label_map)
        max = -1
        result = -1
        for label in label_map:
            if label_map[label] > max:
                max = label_map[label]
                result = label
        return result

if __name__ == "__main__":
    iris = load_iris()
    X = iris.data 	# features
    y = iris.target  # labels

    # print(X.shape)
    # print(y.shape)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42)

    # print(X_train)
    # print(y_train)

    neigh = KNNClassifier(X_train, y_train, 5) # create model (traning stage)
    y_pred = neigh.predict(X_test) 			# prediction
    print(classification_report(y_test, y_pred, target_names=[
        'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))

    from sklearn.neighbors import KNeighborsClassifier
    neigh = KNeighborsClassifier(n_neighbors=20) # build model
    neigh.fit(X_train,y_train) # training
    y_pred_v2 = neigh.predict(X_test)
    print(classification_report(y_test, y_pred_v2, target_names=[
        'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))