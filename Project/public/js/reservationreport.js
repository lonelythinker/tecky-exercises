async function getReservationList(){

    const res = await fetch('/reservationlist', {
        method: "GET"
      });
      const reservationlist = await res.json();
      console.log(reservationlist)

      const reservelist = document.querySelector('#bookreservationlist');
      reservelist.innerHTML =""

      for(let reserve of reservationlist){
          console.log(reserve)
        reservelist.innerHTML += 
    `         <div id="reservationrow">
                <tr attribute-id="${reserve.id}">       
                    <th>${reserve.id}</th>
                    <td>${reserve.book_title}</td>
                    <td>${reserve.name}</td>
                    <td>${reserve.phone}</td>
                    <td>${reserve.email}</td>
                </tr>
              </div>          
    `
      }
}
getReservationList()