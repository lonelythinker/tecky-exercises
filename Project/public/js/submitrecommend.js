

document.querySelector("#submitrecommend").addEventListener('submit',async function(event){
    event.preventDefault();

    // Serialize the Form afterwards
    const form = event.target;
    const formData = new FormData();
    formData.append('title',form.title.value);
    formData.append('author',form.author.value);
    formData.append('isbn',form.isbn.value);
    formData.append('publisher',form.publisher.value);
    formData.append('retailprice',form.retailprice.value);
    formData.append('book_description',form.book_description.value);
    formData.append('image',form.image.value);
    formData.append('image',form.image.files[0]);

    const res = await fetch('/submitrecommend',{
        method:"POST",
        body:formData 
    });

     const result = await res.end();
     console.log(result)
     
})


 