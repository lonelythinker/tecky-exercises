
async function getBookRecommentList() {

  const res = await fetch('/bookrecommends', {
    method: "GET"
  });
  const books = await res.json();
  
  const reportList = document.querySelector('#bookrecommentlist');
  reportList.innerHTML = ""

  for (let book of books) {                                        //toggle即開關一按就觸發modal  //target就係html果邊對應會飄出黎既window 
    //console.log(book.book_description)                           //VVVVVVVVVVVVVVVVVVVVVVVVVV  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
    reportList.innerHTML += `
                  <tr attribute-id="${book.id}">       
                    <th>${book.id}</th>
                    <td>${book.book_title}</td>
                    <td>${book.isbn}</td>
                    <td>${book.clickrate}</td>
                    
                    
                    <td>                                                  
                    <a class="btn btn-sm btn-info"  href="#" data-id1="${book.id}" data-toggle="modal" data-target="#detailModal">
                    <i class="far fa-info"></i>details
                    </a>
                    </td>

                    <td>                                                  
                    <a class="btn btn-sm btn-primary" href="#" data-id3="${book.id}" data-toggle="modal" data-target="#detailModal2">
                    <i class="far fa-edit"></i>edit
                    </a>
                    </td>

                    <td><a class="btn btn-sm btn-danger" href="#" data-id2="${book.id}"><i class="fas fa-trash-alt"></i>delete</a></td>
                  </tr>
        `
  }

  reportList.onclick = async function (event) {    //將onclick set落阿媽度
    
    if (event.target.matches('.btn-danger')) {  //如果click中紅色delete制
      
      const id = event.target.getAttribute('data-id2')  //禁中被listening既div，就get個div既attribute，個名自定
      await fetch(`/bookdelete/${id}`, {            
        method: "DELETE"
      })
      getBookRecommentList()  // load多次個list, 顯示delete後的效果

    } else if (event.target.matches('.btn-info')) {  //如果click中綠色details制

      const id = event.target.getAttribute('data-id1')
      
      const res = await fetch(`/bookrecommendDetails/${id}`, {
        //^^^^^^^記得用個res接返fetch要既response!!    
        method: "GET"  
      });
        const bookdetails = await res.json()
        console.log(bookdetails[0])
        console.log(bookdetails[0].created_at) 
        console.log(bookdetails[0].updated_at)  

      document.querySelector('#titleofdetails').innerHTML =
       `
       <div>Book Title : ${bookdetails[0].book_title}</div>
       `

      document.querySelector('#detailDev').innerHTML = 
       `
       <div><img src="/uploads/${bookdetails[0].book_cover}" class="img-fluid" alt="bookcover"></div>
       <div>- ISBN : ${bookdetails[0].isbn}</div>
       <div>- Author : ${bookdetails[0].author}</div>
       <div>- Publisher : ${bookdetails[0].publisher}</div>
       <div>- Price : $${bookdetails[0].price}</div>
       <div>- Created @ ${bookdetails[0].created_at}</div>
       <div>- Updated @ ${bookdetails[0].updated_at}</div>
       <div>- Book Description : ${bookdetails[0].book_description}</div>
       `

    } else if (event.target.matches('.btn-primary')) {  //如果click中藍色edit制

      const id = event.target.getAttribute('data-id3')
      const res = await fetch(`/bookrecommendDetails/${id}`, {
        method: "GET"  
      });
        const bookdetails = await res.json()
           
      document.querySelector('#titleofedit').innerHTML =
       `
        <div>Book ID: ${bookdetails[0].id}</div>
       `
      document.querySelector('#detailDev2').innerHTML = 
       `
       <form action=/updaterecommend/:id' enctype="multipart/form-data" id="updaterecommend" method="POST">
       <div class="form-group">
      
          <div>[Book Title]</div>
          <div><input type="text" name="title" placeholder="${bookdetails[0].book_title}"</div>
          <div>[ISBN]</div>
          <div><input type="number" name="isbn" placeholder="${bookdetails[0].isbn}"></div>
          <div>[Author]</div> 
          <div><input type="text" name="author" placeholder="${bookdetails[0].author}"></div>
          <div>[Publisher]</div>
          <div><input type="text" name="publisher" placeholder="${bookdetails[0].publisher}"></div>
          <div>[Price HKD]</div>
          <div><input type="number" name="price" step="0.1" placeholder="${bookdetails[0].price}"></div>
          <div>[Description]<div>
          <div><textarea rows="2" style="width:400px; height:120px;" name="description" placeholder="${bookdetails[0].book_description}"></textarea></div>
          
       </div>

       <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" id="submitted">submit</button>
      </div>
      </form>
       `
       //以下係JQuery用黎控制update個submit制!!
       $('#submitted').click(function() {      //btn制的id
        $('#detailModal2').modal('hide');      //modal window的id
      });

       //截edit form既data先fetch to Express
      document.querySelector('#updaterecommend').addEventListener('submit',async function(event){
        event.preventDefault();
        
        //console.log(id)   <---此行是82行的那個book id  

        const form = event.target;
          const formData = new FormData();
          formData.append('title',form.title.value);
          formData.append('isbn',form.isbn.value);
          formData.append('author',form.author.value);
          formData.append('publisher',form.publisher.value);
          formData.append('price',form.price.value);
          formData.append('description', form.description.value);
          
          console.log(form.title.value)

          const res = await fetch(`/updaterecommend/${id}`,{
            method:"PUT",
            body:formData
        });
      });
    }
  }
}
getBookRecommentList()


// $('#submitted').click(function() {
//   $('#detailModal2').modal('hide');
// });

//吸取教訓，太早加event listener!! 當時張dymanic form都仲未load出黎，listen條毛!!
//VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
// document.querySelector('#updaterecommend').addEventListener('submit',async function(event){
//   event.preventDefault();

//   const form = event.target;
//     const formData = new FormData();
//     formData.append('title',form.title.value);
//     console.log(form.title.value)

// })