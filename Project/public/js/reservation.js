
document.querySelector("#reservation").addEventListener('submit',async function(event){
    event.preventDefault();    //記得打呢句截左先

    const searchParams = new URLSearchParams(window.location.search);
    console.log(searchParams)
    const seller_email = searchParams.get('email');   // 如果有email係url比到

    // Serialize the Form afterwards
    const form = event.target;
    const formData = new FormData();
    formData.append('title',form.title.value);
    formData.append('customername',form.customername.value);
    formData.append('contactnumber',form.contactnumber.value);
    formData.append('email',form.email.value);
    
    //send DATA比SQL入reservation form
    const res = await fetch('/reservation',{
         method:"POST",
         body:formData 
     });
    
    // send DATA比mailgun           VV如有seller id可加係url, 咁就知係邊間書店既留書
    const res2 = await fetch('/email',{
        method:"POST",
        body:formData
    })
    const resOfMailGun = await res2.json()
    console.log(resOfMailGun)    

    //成功後顯示
    document.querySelector('#reserved-message').classList.remove('d-none');

    //  const data = await res.json(result);
    //  console.log(data.rows)
})