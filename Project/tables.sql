create table sellers(
id serial primary key,
name varchar(255) not null,
username varchar(255) not null,
password varchar(255) not null,
seller_type varchar(255) not null,
email varchar(255) not null,
phone integer not null,
address varchar(255)
); 

create table books(
id serial primary key,
book_title varchar(255),
author varchar(255),
publisher varchar(255),
isbn integer,
price decimal (5,1),
book_description text,
book_cover varchar(255),
created_at timestamp,
updated_at timestamp,
sellers_id integer,
clickrate integer,     --後加
foreign key (sellers_id) REFERENCES sellers(id)
);

create table reservation(
id serial primary key,
name VARCHAR(255),
phone integer,
email VARCHAR(255),
book_title VARCHAR(255),
book_platform_id integer,
created_at timestamp,
updated_at timestamp,
foreign key (book_platform_id) REFERENCES book_platform(id)
);

create table counters(
id serial,
book_id integer,
book_clickrate integer,
seller_clickrate integer,
overall_rate integer
);

--alter table book_platform add column clickrate integer;
--await client.query(`insert into bookreviews (title,author,isbn) values ('${req.body.title}','${req.body.author}','${req.body.ISBN}');`)