import express from 'express'
import expressSession from 'express-session'
import {Client} from 'pg'
import dotenv from 'dotenv'
import path from 'path'
import bodyParser from 'body-parser'
import multer from 'multer'
import { NodeMailgun } from 'ts-mailgun';

dotenv.config();

const app = express()
const port = 8080

// Benny-1. ts-mailgun settings
const mailer = new NodeMailgun();
mailer.apiKey = 'a78cced1dd15366f6eabc58d51e59502-3e51f8d2-d1cf3ea8';
mailer.domain = 'sandbox7239a73a49f047fa9bb2d3386c266e7c.mailgun.org';
mailer.fromEmail = 'noreply@sandbox7239a73a49f047fa9bb2d3386c266e7c.mailgun.org';
mailer.fromTitle = 'Read The World - Book Reservation Notice';
mailer.init();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
const upload = multer({storage})

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname,'/public')));

//Session
app.use(expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave:true,
  saveUninitialized:true
}));

//SQL Connection
export const client = new Client({
    user:process.env.DB_USERNAME,     //project
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    port:5432
});

client.connect()

//Session Testing route
app.get('/session', (req,res)=>{
  if(req.session){
    console.log(req.session.counter)
    req.session.counter += 1;
  }
  res.end("Session res")
})

//Benny-2. 書商提交書介 & 檢查重複書號
app.post('/submitrecommend',upload.single('image'),async (req,res)=>{
  
  const isbntable = await client.query(`select isbn from books;`)
  const allISBN = isbntable.rows
  
  for(let isbn of allISBN){
    //console.log(isbn)
        if(isbn.isbn && isbn.isbn == req.body.isbn){
          //console.log({'Result':'ISBN Duplicated'})
          res.status(400).json({'Result':'ISBN Duplicated'})
          return
        }
  }
  //console.log(req.file)
  await client.query(`insert into books (book_title, author, isbn, publisher, price, book_description, book_cover, created_at, updated_at) values ('${req.body.title}','${req.body.author}',${req.body.isbn},'${req.body.publisher}','${req.body.retailprice}','${req.body.book_description}','${req.file.filename}','NOW()', 'NOW()');`)

})

//Benny-3. 讀者留書
app.post('/reservation', upload.single('image'),async (req,res,next)=>{
  await client.query(`insert into reservation (book_title, phone, email, name, created_at) VALUES ('${req.body.title}', '${req.body.contactnumber}', '${req.body.email}', '${req.body.customername}','NOW()');`)
  const result = await client.query(`select * from sellers`)

  res.json(result.rows[0])
  //next()  不需要，因為有2封信入，server就要答兩次
})

//Benny-4. MailGun, Andrew幫忙後顯示成功
app.post('/email', upload.single('image'),async (req,res,next)=>{
  //const shopname = await client.query(`select name from sellers where id = ${req.params}`)
  console.log(req.body.email)
    mailer
        .send(`${req.body.email}`, 'Book Reservation Notice', `你好${req.body.customername}, 預留書本︰${req.body.title} 已經代通知書商，請等候書商聯絡作實，謝謝。`)
        .then(()=>(res.json({'success': "true"})))
        .catch((error)=>res.sendStatus(500))

    mailer     //kinmingsghk@gmail.com
        .send(`lonelythinker@gmail.com`, 'Book Reservation Notice', `書商你好，讀者${req.body.customername}想預留書本︰${req.body.title} 請盡快至電${req.body.contactnumber}回覆讀者安排，謝謝。`)
        //.then(()=>(res.json({'success': "true"})))
        .catch((error)=>res.sendStatus(500))       
});

//Benny 5. Get Readers Reservation List to seller's reservation 
app.get('/reservationlist', async (req,res)=>{
  const result = await client.query(`select * from reservation order by created_at desc;`)
  console.log(result.rows)
  res.json(result.rows)
})  

//Benny 6. Get Book Recommend list to seller's dashboard
app.get('/bookrecommends',async (req,res)=>{
  const result = await client.query(`select * from books order by id desc`)
  res.json(result.rows)
  return
})

//Get Book Recommend DETAILS to seller's report 
app.get('/bookrecommendDetails/:id', async (req, res)=>{
  console.log("Book ID: " + req.params.id)
 
  const bookdetails = await client.query(`select * FROM books WHERE id = ${req.params.id}`)
  res.json(bookdetails.rows) // 射返結果比dashboard.js再砌返一個「details page出黎」
  //res.redirect('/')
});

//Update Book Recommend
app.put('/updaterecommend/:id', upload.single('image'), async (req,res)=>{
  console.log(req.body)
  console.log(req.params.id)
  await client.query(`UPDATE books set (book_title, isbn, author, publisher, price, book_description, updated_at) =
                      ('${req.body.title}', ${req.body.isbn}, '${req.body.author}', '${req.body.publisher}', '${req.body.price}', '${req.body.description}', 'NOW()') 
                      WHERE id = ${req.params.id};`)
  res.json({success: true})
})

//Delete Book Recommendation from seller's dashboard page
app.delete('/bookdelete/:id', async (req,res)=>{    //:id係placeholder
  console.log("Book ID Deleted: " + req.params.id)
  await client.query(`DELETE FROM books WHERE id = ${req.params.id}`)
  res.json({Delete: 'Success'})
  return  
});






//Add clickrate to counter for book_clickrate AND seller_clickrate   //未測試!!
app.put('/addclickrate/:book_id/:seller_id', async (req,res)=>{      //等Ming哥main page既book card內load入bookid及sellerid
    const result = await client.query(`select book_id from counters`)
    const bookids = result.rows
    for(let bookid of bookids){
      if(bookid !== req.params.book_id){  //唔等於即係新書bookid
          await client.query(`insert into counters (book_id,seller_id,book_clickrate) VALUES (${req.params.book_id}, ${req.params.seller_id},1)`)
        }else{
          await client.query(`update counters SET book_clickrate = book_clickrate + 1 WHERE book_id = ${req.params.book_id}`)
      }
      res.json({result: true})
    }    
})

//較靚仔既死法
app.use(function(req,res){
  res.sendFile(path.join(__dirname,'public/404.html'));
})

app.listen(port,()=>{
    console.log("Listening at localhost:8080")
})
