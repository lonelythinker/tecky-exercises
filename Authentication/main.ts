import express, { Request, Response, NextFunction } from 'express';
import expressSession from "express-session";
import bodyParser from "body-parser";
import pg from 'pg';
import dotenv from 'dotenv';
import bcrypt from 'bcryptjs';
dotenv.config();

const SALT_ROUNDS = 10;
export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};

export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

export const client = new pg.Client({
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,  //users
    host: "localhost",
    port: 5432
});

const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave: true,
    saveUninitialized: true
}));

client.connect()

/*  //Non-Hash version
app.post('/login', async (req, res) => {
    console.log(JSON.stringify(req.body))
    const result = await client.query(`select * from users`)
    console.log(result.rows)
    const users = result.rows

    for (let user of users) {
        if (user.username === req.body.username && user.password === req.body.password) {
            if (req.session) {
                req.session.user = user
            }
            res.status(200).redirect('admin.html')
            return    //記得加返return, 成功左redirect去admin.html後就停止，如果唔係會再走去行redirect fail
        } 
        //res.status(401).redirect('/?fail-Login')  如果呢行就redirect('/?fail-Login')，即係for loop左第一個user如果唔岩就redirect，所以會出現下面所述既現象
    }
    res.status(401).redirect('/?fail-Login')    //如果呢行放左係 line 41, 會出現只有第一個user才能登入的現象。
})
*/

//Hash Version
app.post('/login', async (req, res) => {
    console.log(JSON.stringify(req.body))
    const result = await client.query(`select * from users`)
    console.log(result.rows)
    const users = result.rows

    for (let user of users) {
        if (user.username === req.body.username && await checkPassword(req.body.password,user.password)){
            if (req.session) {
                req.session.user = user
            }
            res.status(200).redirect('admin.html?`${req.session.user}`')
            return    
        } 
    }
    res.status(401).redirect('/?fail-Login')  
})

const isLoggedIn = function (req: Request, res: Response, next: NextFunction) {
    console.log(req.session);
    if (req.session) {
        if (req.session.user) {
            next();
            return;
        }
    }
    res.redirect('/');
}

app.use(express.static('public'));
app.use(isLoggedIn, express.static('protected'));
app.listen(8080, function () {
console.log("Express listening at http://localhost:8080");
})