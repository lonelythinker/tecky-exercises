interface shoot{
    damage:number
}
class Glock implements shoot{
    public damage:number
    public name:string
    constructor(name:string){
        this.damage = 7;
        this.name = name;
    }
}

class Revolver implements shoot{
    public damage:number
    public name:string
    constructor(name:string){
        this.damage = 10;
        this.name = name;
    }
}

class Fist implements shoot{
    public damage:number
    public name:string
    constructor(){
        this.damage = 1;
        this.name = "Iron Fist"
    }
}

class bbgun implements shoot{
    public damage:number
    public name:string
    constructor(){                 //如果呢度唔打public, 要係上一行打返 power:number
        this.damage = 2
        this.name = "BBGUN"
    }
}

interface player{
    
} 

class soldierTypeA implements player{
    name: string
    hp:number
    weapon1
    weapon2
    weapon3
    useweapon1:boolean

    constructor(){
        this.weapon1 = new Revolver("Revolver.45")
        this.weapon2 = new bbgun()
        this.weapon3 = new Fist()
        this.hp = 100
        this.useweapon1 = true
    }

    shoot(){
        if(this.useweapon1){
            if(Math.random()>0.85){
                godzilla.beingattack((this.weapon1.damage + this.weapon2.damage)*2)
                // console.log(`Soldier A shot target with ${this.weapon1.name} and caused ${this.weapon1.damage} damage!`)     //呢個this指clockholder/revolver, 睇下邊個call shoot()
                // console.log(`Soldier A shot target with ${this.weapon2.name} and caused ${this.weapon2.damage} damage!`)
                console.log(`Soldier A fired both weapons and caused ` +  ((parseInt(this.weapon1.damage) + parseInt(this.weapon2.damage))*2) + ` damage-[DOUBLE DAMAGE]`)
                console.log(godzilla.reportHP())
            }else{
                godzilla.beingattack(this.weapon1.damage)
                console.log(`Soldier A shot target with ${this.weapon1.name} and caused ${this.weapon1.damage} damage!`)
                console.log(godzilla.reportHP())
        }
        }else{
            godzilla.beingattack(this.weapon3.damage)
            console.log(`Soldier A shot target with ${this.weapon3.name} and caused ${this.weapon3.damage} damage!`)
            console.log(godzilla.reportHP())
        }
                 
    }
    switchweapon(){
        if(this.useweapon1){
            this.useweapon1 = false
        }else{
            this.useweapon1 = true
        }
    }
}

class soldierTypeB implements player{
    name: string
    hp:number
    weapon1
    weapon2
    weapon3
    useweapon1:boolean

    constructor(){
        this.weapon1 = new Glock("Glock7")
        this.weapon2 = new bbgun()
        this.weapon3 = new Fist()
        this.hp = 100
        this.useweapon1 = true

    }

    shoot(){
        if(this.useweapon1){
            if(Math.random()>0.85){
                godzilla.beingattack((this.weapon1.damage + this.weapon2.damage)*2)
                // console.log(`Soldier B shot target with ${this.weapon1.name} and caused ${this.weapon1.damage} damage!`)     //呢個this指clockholder/revolver, 睇下邊個call shoot()
                // console.log(`Soldier B shot target with ${this.weapon2.name} and caused ${this.weapon2.damage} damage!`)
                console.log(`Soldier B fired both weapons and caused ` +  ((parseInt(this.weapon1.damage) + parseInt(this.weapon2.damage))*2) + ` damage-[DOUBLE DAMAGE]`)
                console.log(godzilla.reportHP())
            }else{
                godzilla.beingattack(this.weapon1.damage)
                console.log(`Soldier B shot target with ${this.weapon1.name} and caused ${this.weapon1.damage} damage!`)
                console.log(godzilla.reportHP())
        }
        }else{
            godzilla.beingattack(this.weapon3.damage)
            console.log(`Soldier B shot target with ${this.weapon3.name} and caused ${this.weapon3.damage} damage!`)
            console.log(godzilla.reportHP())
        }
                 
    }
    switchweapon(){
        if(this.useweapon1){
            this.useweapon1 = false
        }else{
            this.useweapon1 = true
        }
    }
}

export class Monster{
    name:string;
    life:number;

    constructor(life:number){
        this.name = "godzilla";
        this.life = life;
    }

    beingattack(hurt:number){
        if(hurt>this.life){
            this.life = 0
        }else{
            this.life -= hurt
        } 
    }

    reportHP(){
        return `godzilla still has hp: ${this.life}`
    }
}

const revolver = new soldierTypeA()
const glocker = new soldierTypeB()

const godzilla = new Monster(200)

while(godzilla.life>0){
    revolver.shoot()
    revolver.switchweapon()
    glocker.shoot()
    glocker.switchweapon()
}