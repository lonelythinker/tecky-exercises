class Student{
    /*Fields*/
    name:string;
    age:number;
    learningLevel:number; 

    /*Constructor*/
    constructor(name:string,age:number, learingLevel:number){
        this.name = name;
        this.age = age;
        this.learningLevel = learingLevel;                //inherit此class的subtype by default個learningLevel都係 0 開始
    }

    /* Methods */
    learn(hourSpent:number){
        this.learningLevel += hourSpent * 0.3 
    }

    slack(hourSpent:number){
        this.learningLevel -= hourSpent * 0.1
    }
}
//---------------------------------------------------------------
const student01 = new Student("Benny", 40, 0)

console.log(student01)
student01.age = 41
console.log(student01)
student01.learningLevel = 100       // 先係update +100
student01.learn(100)                // 再update +30 (100*0.3)
student01.learn(30)                 // 再update +9 (30*0.3)
console.log(student01)              // 會累積而非新既覆蓋舊的

//---------------------------------------------------------------
class SelfLearnStd extends Student{
    // name: string;                  //唔寫都ok, 因為會由Student承繼
    // age: number;
    // learningLevel: number;
       switchLevel: number;              //有新attribute加就寫呢度

    constructor(name: string, age:number, learningLevel:number){
        super(name,age,learningLevel);
        this.learningLevel = 0;
        this.switchLevel = 4;       
        
    }

    learn(hourSpent:number){
        this.learningLevel += hourSpent**2    //SelfLearnStd此subtype重新自定義method learn
    }

    playswitch(hourSpent:number){
        this.switchLevel += hourSpent*2
    }
}
//---------------------------------------------------------------

const selflearn01 = new SelfLearnStd("Barbara", 29, 0)

selflearn01.learn(12)                          //12 **2
selflearn01.playswitch(4.5)                    //4.5 * method的 2 + 預設的 5
console.log(selflearn01)

//----------------------------------------------------------------

class SelfLearnStdAtHome extends SelfLearnStd{
    // name: string;
    // age: number;
    // learningLevel: number;
       pcLevel: number;

    constructor(name: string, age:number,learningLevel:number){  
        super(name,age,learningLevel);
        this.pcLevel = 0;
                                                       // constructor中冇特別寫switchLevel,但已由 SelfLearnStd
        //this.switchLevel = 99;                        // 如果有再寫新既switchKLevel係constructor, 就會overide左承繼果個
    }                                                               

    learn(hourSpent:number){
            super.learn(hourSpent)                                // call母class, SelfLearnStd的learn() method黎用
    }
    
    playpc(hourSpent:number){
            this.pcLevel = this.pcLevel + hourSpent*0.5
    }

    playswitch(hourSpent:number){      // 
        super.playswitch(hourSpent)    //用super call SelfLearnStd果個playswitch method,即this.switchLevel += hourSpent*2
        this.switchLevel = this.switchLevel + hourSpent*3          // SelfLearnStdAtHome再定義playswitch method
    }                                                              // SelfLearnStd個constructor預設switchLevel已有 5
}                                                                  // 故athomestd01.playswitch(100)就有5 + 200 + 300=505!
//-----------------------------------------------------------------------------------------------------
console.log(SelfLearnStdAtHome)
const athomestd01 = new SelfLearnStdAtHome("Dick", 39, 0)

athomestd01.learn(8)              // 8 **2 super call了 SelfLearnStd 的method line 49.
athomestd01.playswitch(100)       // 4 + 200 + 300 = 505
athomestd01.playpc(14)
console.log(athomestd01)

//------------------------------------------------------------------------------------------------------
class BankAccount{
    // Bank Account Balance
     private balance:number;
     

    constructor(){
        this.balance = 10;
        this.balance = 1000000;       //如重複declare, 以下面的為準
    }

    deposit(amount:number){
        this.balance += amount;
    }

    withdraw(amount:number){
        if(this.balance < amount){
            throw new Error("Not enough balance!");
        }
        this.balance -= amount;
    }

    
}

const account = new BankAccount();
account.deposit(100);
console.log(account)
//account.withdraw(110);

account.withdraw(5000)
console.log(account)

// This line will throw an error.