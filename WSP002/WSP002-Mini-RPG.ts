class Player{
    name:string;
    strength:number;

    constructor(name:string, strength:number){
        this.name = name;
        this.strength = strength;
    }

    attack(monster){
        while(monster.life > 0){
            if(Math.random()>0.85){
                monster.beingattack(this.strength*2)
                console.log(`Player ${player.name} attacks a monster (hp: ${monster.life}) [CRITICAL]`)  
            }else{
                monster.beingattack(this.strength)  
                console.log(`Player ${player.name} attacks a monster (hp: ${monster.life})`)
            }
            this.gainExperience()
        }
        if(monster.life<=0){
            console.log(`You have killed the monster!`)
        }
    }

    gainExperience(){
          this.strength += 1
    }
}

class Monster{
    name:string;
    life:number;

    constructor(life:number){
        this.name = "スライム";
        this.life = life;
    }

    beingattack(hurt:number){
        if(hurt>this.life){
            this.life = 0
        }else{
            this.life -= hurt
        } 
    }
}

const monster = new Monster(100)
const player = new Player("Benny", 10)

console.log(player)
console.log(monster)

player.attack(monster)

/*
Player { name: 'Benny', strength: 10 }
Monster { name: 'スライム', life: 100 }
Player Benny attacks a monster (hp: 90)
Player Benny attacks a monster (hp: 79)
Player Benny attacks a monster (hp: 55) [CRITICAL]
Player Benny attacks a monster (hp: 42)
Player Benny attacks a monster (hp: 28)
Player Benny attacks a monster (hp: 13)
Player Benny attacks a monster (hp: 0)
You have killed the monster!
*/