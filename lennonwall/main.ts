//import expressSession from 'express-session'
//import * as bodyParser from 'body-parser';
import jsonfile from 'jsonfile'
import multer from 'multer'
//import http from 'http';
//import socketIO from 'socket.io';
import express from 'express';

//socketIO唔係同express完全有關，但經常周express一齊用

//轉去lennon_wall project繼續

const port = 8080
const app = express()

const storage = multer.diskStorage({
     destination: function (req, file, cb) {
       cb(null, `${__dirname}/public/uploads`);
     },
     filename: function (req, file, cb) {
       cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
     }
   })
                      
const upload = multer({storage})

app.use(express.static('public'))
//app.use(bodyParser.urlencoded({extended:true}));  //處理body中的漢字
//app.use(bodyParser.json());   //處理body中的json

app.get('/memos',async function(req,res){
    const memos = await jsonfile.readFile('./memos.json');
    res.json(memos);
})

//add memo + upload file
app.post('/add-memo',upload.single('image'),async function(req,res){
    const content = req.body.content                             //{ content:'lennonmemoText'}
    const memos = await jsonfile.readFile('./memos.json');       //先讀memos.json落variable, 第一次讀前要先手動加定個[]先
    let maxMemoID:number = 0;
    for(let memo of memos){
        if(memo.id > maxMemoID){
            maxMemoID = memo.id;
        }
    }   
    memos.push({
                id: maxMemoID + 1, //每次upload都有個unique比返
                content,                                         //content係memo上的文字   
                image: req.file.filename    //只係filename, 咁實體邊度upload比server?    //upload file時，req.file中記錄到的檔案名
              })                                                 //將新content及file名加入memos variable
    await jsonfile.writeFile('./memos.json',memos,{spaces:4})    //再將memos variable寫入memos.json
    res.json({success:true})                                            //寫完馬上redirect至首頁    
})

// edit memo
app.put('/memos/:id',async (req,res)=>{  //點解會有個:id係度?
    const memoId = parseInt(req.params.id);
    if(isNaN(memoId)){
        res.status(400).json({msg:"id is not a number!"});
        return;
    }
    const memos = await jsonfile.readFile('./memos.json');

    const {content} = req.body;

    //const updatedMemos = memos.filter((memo:any)=>memo.id !==memoId)  //唔明  
    for(let memo of memos){
        if(memo.id === memoId){
            memo.content = content;
            break;
        }
    }
    await jsonfile.writeFile('./memos.json',memos,{spaces:4});
    res.json({success:true});
});

//delete memo, 完全睇唔明段code做緊乜
app.delete('/memos/:id',async (req,res)=>{   //點解會有個:id係度?
    const memoId = parseInt(req.params.id);
    if(isNaN(memoId)){      //咩可能會出現係數字?
        res.status(400).json({msg:"id is not a number!"});
        return;
    }
    const memos = await jsonfile.readFile('./memos.json');

    const deletedMemos = memos.filter((memo:any)=>memo.id !== memoId);
    await jsonfile.writeFile('./memos.json',deletedMemos,{spaces:4});
    res.json({success:true});
});

app.listen(port,function(){
    console.log(`Listening at local host ${port} port`)
})