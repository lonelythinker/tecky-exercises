document.querySelector("#addmemoform")
.addEventListener('submit', async (event)=>{
    event.preventDefault();     //鬼鬼鼠鼠係背後做左d野而唔成頁reload就係AXAJ
    const form = event.target   //why do this?
    const formData = new FormData()  //裝相用?
    
    formData.append('content',form.content.value)    //??
    formData.append('image', form.image.files[0]) //??
    
    const res = await fetch('/add-memo',{      //fetch到底個流程係點? res未係data?
        method: "POST",
        // header: {                                 // 有formData就唔駛寫header? why?
        //     "Content-type":"application/json"
        // },                                             //唔係靠html <form action="/add-memo"... 而係用fetch對lennon memo上去server而唔觸發Express既handler
        // body:JSON.stringify(formObject)
        body:formData
})
        const result = await res.json()                  //res.json()又assign比result, 但result又愛黎做乜?
        document.querySelector("#add-memo-message")
                .classList.remove('d-none')
   
        loadmemos()        //submit完memo後data係memos.json, 要再行多次loadMemos個fns先會出黎  
})

//係client side行js load定個memoS 而唔需要reload頁面
async function loadmemos(){
        const res = await fetch('/memos')   //用fetch去'/memos', 其實都要係express server寫定個handler, read memos.json再射比client
        const memos = await res.json()
        console.log(memos)

        const memoContainer = document.querySelector(".memo-container")
        memoContainer.innerHTML = ""    //每次加左新memo要成個memos loop多次再即時編寫page, 咁要先將之前既顯示清空，否則會累加重複   
        
        for(let memo of memos){               //load完 memos data先再即時編寫個page呈現比client
                memoContainer.innerHTML += ` 
                <div class="memo" contenteditable>                      
                        ${memo.content}
                        <img src="/uploads/${memo.image}" alt="memophoto" class="img-fluid" />
                        <div class="icon bottom-right editmemo" data-id="${memo.id}">
                            <i class="fas fa-edit"></i>   
                        </div>
                        <div class="icon top-right delmemo" data-id="${memo.id}">
                            <i class="fas fa-trash-alt"></i>
                        </div>
                </div>
        `
        }

        memoContainer.onclick = async function(event){           
                if(event.target.matches('.delmemo')){ //阿媽阿仔兩個class都加, 但次後又因為fa-edit冇id又唔好加阿仔，好亂，唔知發生咩事
                        const id =event.target.getAttribute('data-id'); //自創的attribute可以咁樣攞返
                        await fetch(`/memos/${id}`,{    //射去server等/memo/:id 個handler處理
                                method: "DELETE"
                        });     
                        loadmemos()
                }else if(event.target.matches('.editmemo')){
                        const id =event.target.getAttribute('data-id');
                        const parentMemo = event.target.closest('.memo');  //唔明做乜要呢句
                        const res = await fetch(`/memos/${id}`,{    
                                method: "PUT",
                                headers:{
                                        "Content-Type":"application/json"    //fetch要加返headers註明content-type
                                },
                                body:JSON.stringify({
                                        content: parentMemo.innerText
                                })   //what happen here?
                        });
                        const result = await res.json();    //唔明
                }      
        }
}

loadmemos()

//創造一個attribute比icons, 咁就可以link到id#同tag