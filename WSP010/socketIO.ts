//import http from 'http';  //nodejs built-in 的 http??
//import socketIO from 'socket.io';
import express from 'express';
import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();

//socketIO唔係同express完全有關，但經常周express一齊用
//嚴格而言socket.io並非HTML 

export const client = new Client({
    user:process.env.DB_USERNAME,   //wsp010
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    port:5432
});

client.connect()

const app = express()
app.use(express.static('public'))

app.get('/students',async function(req,res){
    const result = await client.query(`select * from students;`)
    //console.log(result.rows)
    res.json(result.rows)   //一定係咁寫，冇json、冇.rows都會爆
    //跟住frontend需要AJAX @ index.js
})

//client.end()  <--- 如果手多加左呢句會出err,  上面SQL未讀完data! UnhandledPromiseRejectionWarning: Error: Client was closed and is not queryable

app.listen(8080, function(){
    console.log("Listening at http://localhost:8080")
})