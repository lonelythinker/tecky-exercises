

async function fetchStudents(){
    const response = await fetch('/students')   //去trigger backend express中 '/students'路徑並取得response回來的data
    
    const students = await response.json()    //一個能夠實現 (resolve) 把回傳的結果的 body text 解析成 JSON 型別的 Promise 

    const studentList = document.querySelector("#students")  // 預備好接受server response的HTML位置
    
    for(let student of students){
        studentList.innerHTML += `
        <div>
            id: ${student.id}</br>
            name: ${student.name}<br/>
            level: ${student.level}<br/>
            date of birth: ${student.date_of_birth}<br/>
        </div>
        <hr>
        `
    }
}

fetchStudents()
