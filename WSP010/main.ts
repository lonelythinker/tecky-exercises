import {Client} from 'pg';
import dotenv from 'dotenv';
dotenv.config();

export const client = new Client({
    user:process.env.DB_USERNAME,     //wsp010
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    port:5432
});

async function main(){

await client.connect()

    //const result = await client.query(`select * from teachers right outer join students on students.teacher_id = teachers.id;`)
    //const result = await client.query(`select teachers.id as teacher_id, students.* from students left outer join teachers on students.teacher_id = teachers.id;`) 
    //const result = await client.query(`select * from teachers;`)
    //const result = await client.query(`select * from students;`)
    await client.query(`insert into teachers (name,date_of_birth) values ('dick','1980-10-10');`)
    // await client.query(`insert into students (name,level,date_of_birth,teacher_id) VALUES 
    //                                             ('Peter',25,'1995-05-15',(SELECT id from teachers where name ='Bob')),
    //                                             ('John',25,'1985-06-16',(SELECT id from teachers where name ='Bob')),
    //                                             ('Simon',25,'1987-07-17',NULL);`)
    //console.log(JSON.parse(JSON.stringify(result.rows)))

await client.end()

}
main()