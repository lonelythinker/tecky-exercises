import fs from 'fs'
import path from 'path'

function readdirPromise(folderPath:string){
    return new Promise<string[]>(function(resolve,reject){     /* Promise<>, 意即Promise最後會比返個咩type既資料出黎，呢度係string[],一個array裝住多個string */
        fs.readdir(folderPath, function(err, files){
            if(err){
                reject(err.message)
            }resolve(files)
        })
    })
}

function statPromise(filePath:string){
    return new Promise<fs.Stats>(function(resolve,reject){     
        fs.stat(filePath, function(err, stats){     //其實打hover落stats出個type落Promise<>內面咪一定岩?
            if(err){
                reject(err.message)
            }resolve(stats)
        })
    })
}

export async function readJSFiles (folderPath:string){

    let files = await readdirPromise(folderPath)    
    for(const file of files){
        const stats = await statPromise(path.join(folderPath, file))  
        if(stats.isFile() && file.endsWith('js')){      
            //console.log(path.join(path.resolve(folderPath, file)))    如果呢度有console.log, 會頂走
        }else if (stats.isDirectory()){
            await readJSFiles(path.join(folderPath, file))          //await唔await有咩分別？
        }
    }
}
readJSFiles('./')