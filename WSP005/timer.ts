/*
const count = function (text:string,s:number){
    let a = 0;
    let timer = setInterval(function(){
      console.log(`${text}${a}`);
      a = a + 1;
      if(a>5){
        clearInterval(timer);
      }
    },s);
  };
  
  console.log(1); 
  count('haha', 1000);  //text, seconds
  console.log(2);
*/

~async function(){      // ~ 係咩意思？
  const count = function (text:string,second:number){
      return new Promise (resolve=>{
    let a = 0;
    let timer = setInterval(function(){
        console.log(`${text}${a}`);
        a = a + 1;
        if(a>5){
          clearInterval(timer);
          resolve()
        }
      },second)
    })
  }

console.log(1); 
await count('haha', 1000);  //text, seconds
console.log(2);

}()
