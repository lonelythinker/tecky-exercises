import readline from 'readline' 

/* 有問題的readline promisfy版本，問完一次問題就唔識再問

const readLineInterface = readline.createInterface({                
    input: process.stdin,
    output: process.stdout
})

export function readLinePromise(question:string){        
    return new Promise<string>(function(resolve, reject){
        
        readLineInterface.question(question, function(answer){
            resolve(answer)
            readLineInterface.close()
        })
    })
}
*/


/* 正確的readLine promisfy版本*/
export function readLinePromise(question:string){        
    return new Promise<string>(function(resolve, reject){
        
        const readLineInterface = readline.createInterface({                
            input: process.stdin,
            output: process.stdout
        })

        readLineInterface.question(question, function(answer){
            resolve(answer)
            readLineInterface.close()
        })
    })
}