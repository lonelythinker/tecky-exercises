import {readJSFiles} from './readJSFiles'
import {readLinePromise} from './readlinePromise'
import fs from 'fs';
import os from 'os'

// Using recursive function for a REPL feel.
const readCommand = async function(){
    while(true){ // game-loop, eval-loop
        // Exit by Ctrl+C
        const answer = await readLinePromise("Please choose read the report(1) or run the benchmark(2):")
        const option = parseInt(answer);  
        
        console.log(`Option ${answer} chosen.`);
         if(option == 1){
             await readTheReport();  
         }else if(option == 2){
             const trialInputs = await readLinePromise("Please enter how many times of trial you want to run:")
             const trialNums = trialInputs.split(',').map(function(abc){
                return parseInt(abc)}                                   //用傳統寫法記得return
                ).filter(num=>!isNaN(num))       //此行不能理解 refer to WSP005 session III 35:20

             await runTheBenchmark(trialNums); 
         }else{
             console.log("Please input 1 or 2 only.");
         }
     }
}

readCommand()

async function runTheBenchmark(trialNums:number[] = [10,30,100]){  //如果paras冇寫‵，by default行[10,30,100]次
    const result:Report = []
    for(let trialNum of trialNums){            //User入左幾個數字係個array度，遂個loop出黎代入runTimes()
        const trial = await runTimes(trialNum)
        result.push(trial)
    }
    /*
    const trial10 = await runTimes(10);
    const trial30 = await runTimes(30);
    const trail100 = await runTimes(100)
    const result:Report = [trial10, trial30, trail100] //result的type是Report, 而Report就係Trial[], 即一組object in an array
    */
    await fs.promises.writeFile('result.json', JSON.stringify(result))
}

async function runTimes(n:number):Promise<Trial> {  //要求function執行完畢後一定要交比一個叫Trial的object, 而此object在下面以interface方式「自行定義」
    const memoryBegin = os.freemem();
    const startTime = new Date();
    for (let i = 0; i < n; i++) {
        await readJSFiles('./'); //如果冇await，nodejs會唔等readJSFiles做完，走去計時間同memory果d野先，於是會出時間0,memory used係0既奇怪野
    }
    const endTime = new Date();
    const memoryCompleted = os.freemem();
    const Duration = endTime.getTime() - startTime.getTime();  //亦可用 + 號將date轉化為number
    const memoryUsed = memoryCompleted - memoryBegin;

    return { startTime: startTime.toISOString(),
             endTime: endTime.toISOString(),
             Duration: Duration,
             memoryUsed: memoryUsed,
             name: `Trial ${n} times`
            };
}

type Report = Trial[]    //Trial[] means multipule Trial objects有果5 pairs in an array 

interface Trial{
    startTime : string
    endTime : string
    Duration : number
    memoryUsed : number
    name : string
}

async function readTheReport(){

    // await fs.promises.readFile('result.json')
    //     .then((data)=>{
    //         console.log(`\n`, data.toString())
    // })

    const buffer = await fs.promises.readFile('result.json')  //唔用.then handler將內容接出黎的話，可以assign個variable裝住佢.
    //console.log(JSON.parse(buffer.toString()))              //做法一︰就咁console.log返個變返JS object既result
    const results:Report = JSON.parse(buffer.toString())      //做法二︰由於results是一個array,可將array中的element遂個loop出來美化. 
    for(const each of results){                               //可自由選擇要顯示的object pairs!  
        console.log(`========= ${each.name} =========`)
        console.log('startTime:' + each.startTime)
        console.log('endTime:' + each.endTime)
        console.log('Duration:' + each.Duration)
        console.log('memoryUsed:' + each.memoryUsed + "\n")
    }
}