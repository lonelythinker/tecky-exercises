export default class Calculator {
    private num : number;

    constructor(initNum : number = 0){
        this.num = initNum;
    }

    add(num: number){
        this.num += num;
        return this.num;
    }

    minus(num : number){
        this.num -= num;
        return this.num;
    }
}