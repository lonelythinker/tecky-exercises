// Version 1
const filtered = [3,5,7,9,11,13,15,17].filter(function(num){
    if(num>10)
    return true
}) 
console.log(filtered)

// Version 2
const filtered2 = [3,5,7,9,11,13,15,17].filter((num)=>{
    if(num>11)
    return true
}) 
console.log(filtered2)

// Version 3
const filtered3 = [3,5,7,9,11,13,15,17].filter(num=>{
    if(num>13)
    return true
}) 
console.log(filtered3)

// Version 4
const filtered4 = [3,5,7,9,11,13,15,17].filter(num=>num>15);
console.log(filtered4)