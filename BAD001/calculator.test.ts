import Calculator from './calculator';

test('it adds a number', () => {
    const calculator = new Calculator();
    expect(calculator.add(5)).toBe(5);
  });
  
  test('it adds a number and go on', () => {
    const calculator = new Calculator();
    expect(calculator.add(5)).toBe(5);
    expect(calculator.add(2)).toBe(7);
  });
  