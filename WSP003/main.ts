import * as fs from "fs";

fs.readFile('quotes.txt',function(err:Error,data:Buffer){
    if(err){
        console.log(err);
        return;
    }
    console.log(data.toString('utf8'));
});


const dijkstraQuote1 = "Computer science is no more about computers than astronomy is about telescopes.\n";

const dijkstraQuote2 = "Simplicity is prerequisite for reliability.\n";

// Flag w overwrites the original content and create the if it does not exist
fs.writeFile('quotes-dijkstra.txt',dijkstraQuote1,{flag:'w'},function(err:Error){
    if(err){
        console.log(err);
    }
        // Flag a+ appends to the content and create the file if it does not exist
        fs.writeFile('quotes-dijkstra.txt',dijkstraQuote2,{flag:'a+'},function(err:Error){
            if(err){
                console.log(err);
            }
        })
})

