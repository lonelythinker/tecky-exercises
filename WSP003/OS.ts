import os from 'os';

console.log(os.homedir());   // cd / , Linux個home directory
console.log(os.hostname())
console.log(os.cpus());      // CPU有幾多core就當有幾多粒CPU
console.log(os.cpus().length);
console.log(os.freemem());
console.log(os.totalmem());