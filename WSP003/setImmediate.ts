function power({ x, y }: { x: number; y: number; }){
    return x**y;
}

setImmediate(function(){
    console.log(`setImmediate run power function = ${power({ x: 2, y: 3 })}`);     //step 2
});

setTimeout(function(){
    console.log(`setTimeout run power function = ${power({ x: 2, y: 3 })}`);      //step 3
},1000);

console.log(power({ x: 2, y: 3 }))    //step 1