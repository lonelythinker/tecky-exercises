import * as cluster from 'cluster';
import * as os from 'os';
import * as fs from 'fs';

const numCPUs  = os.cpus().length;
if(cluster.isMaster){
    console.log(`Master ${process.pid} is running.`);
    for(let i = 0; i < numCPUs ; i++){
        cluster.fork();
    }
    cluster.on('exit',(worker,code,signal)=>{
        console.log(`worker ${worker.process.pid} died`);
    })
}else{

    fs.readFile('quotes.txt',(err,data)=>{
        if(err){
            console.error(err);
        }else{
            console.log(data.toString());
        }
    });
    console.log(`Worker ${process.pid} started`);
}