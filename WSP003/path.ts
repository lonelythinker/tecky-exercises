import * as path from 'path';

const folder1 = "folder1/";
const folder2 = "/folder2/";
const file1 = "file1";
const fileA = "fileA";

// Using path
console.log(path.join("folder1/","/folder2/","fileA","file1"));
// -> folder1/folder2/file1                                 //用path就唔會出現前後兩//痴埋一齊既情況?

// Using string concat
console.log(folder1 + folder2 + file1 + fileA)
// -> folder1//folder2/file1

