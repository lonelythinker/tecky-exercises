import express from 'express'
import expressSession from 'express-session'
import path from 'path'
import bodyParser from 'body-parser'
import multer from 'multer'
import jsonfile from 'jsonfile'

const app = express ()

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

// Add this line          what is this actually??
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

//接收upload files後的重新改名機制
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })

const upload = multer({storage})

// const users = [
//     {
//         username:"gordon@tecky.io",
//         password:"tecky"
//     },
//     {
//         username:"alex@tecky.io",
//         password:"tecky"
//     },
// ]

//Login 
app.post('/login', function(req,res,next){
    const{username, password} = req.body;

    
    console.log(`Username is ${username} and Password is ${password}`)
    console.log(req.body)
    res.end("Login Successful!")
})

//add memo + upload file
app.post('/add-memo',upload.single('image'),async function(req,res){
    const{content} = req.body;
    const memos = await jsonfile.readFile('./memos.json');       //先讀memos.json落variable
    memos.push({content,                                         //content係memo上的文字   
                image: req.file.filename                         //upload file時，req.file中記錄的檔案名
              })                                                 //將新content及file名加入memos variable
    await jsonfile.writeFile('./memos.json',memos,{spaces:4})    //將memos variable寫入memos.json
    res.redirect('/')                                            //寫完馬上redirect至首頁    
})

app.get('/memos', async function(req,res){                       // Exercise 1 part 3
    const memos = await jsonfile.readFile('./memos.json')
    res.json(memos)
})

app.use(function(req,res,next){
    
    if(req.session){
        if(req.session.counter){
            req.session.counter += 1;    
        }else{
            req.session.counter = 1
        }
      console.log(`Counter is ${req.session.counter}`);  
    }
    next()      //一係自己答，一係next比下一個middleware, 唔可以又唔答又唔next, 唔係就loop死
})

app.use(function(req,res,next){
    console.log(`[${new Date().toISOString()}] Request ${req.path}`);   //req.path會比返依家個request攞緊d咩
    next()
})

app.use(express.static('public'));   //乜path都唔寫即係大包圍

app.use(function(req,res){          //最後，pagenotfound handle
    res.sendFile(path.join(__dirname,'public/404.html'))
})

app.listen(8080, function(){
    console.log("Express Listening at http://localhost:8080")
})
