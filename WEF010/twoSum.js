let twoSum = function(nums, target) {
    
        for(const i in nums){
            for(const j in nums){
                let firstNum = nums[i]
                let secNum = nums[j]
                if(firstNum + secNum === target && i !== j){
                    return [i, j]
            }              
        }
    }
}
console.log(twoSum([2,7,11,5,8], 9))