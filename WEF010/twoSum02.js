const twoSum = (nums, target) => {
    const map = {};
    
    for (let i = 0; i < nums.length; i++) {
      const another = target - nums[i];
      
      if (another in map) {
          console.log(another)
          //console.log(map) 存在
        return [map[another], i];
      }
  
      map[nums[i]] = i;
      //console.log(map) 存在
    }
    // console.log(map) 不存在
    return null;
  };
  console.log(twoSum([2,7,11,5,8], 13))