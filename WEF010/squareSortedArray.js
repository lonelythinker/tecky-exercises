var sortedSquares = function(A) {
    let sortedArray = []
    for(const num of A){
        sortedArray.push(num*num) 
    }
    return sortedArray.sort(function(a, b){return a-b})
    
};

console.log(sortedSquares([-4,-1,0,3,10]))