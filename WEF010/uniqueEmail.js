const emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]

let numUniqueEmails = function abc (emails){
    const numofUniqueEmail = new Set()                  // Set只會keep唔同既elements, duplicated的不會keep
for(const email of emails){
    
    const [localName, domainName] = email.split("@")    //split() 後會比返個array
    const processedLN = localName.replace(/\./g,"").replace(/\+.*/,"")   //   /\./g 指所有的.  /\+.*/ 指所有+號以後的東西  \ 指escape char
    const newEmails = `${processedLN}@${domainName}`
    //console.log(newEmails)
    numofUniqueEmail.add(newEmails)                     // 加野入Set用.add(想加既野)
    //console.log(numofUniqueEmail)
}
return numofUniqueEmail.size
}
console.log(numUniqueEmails(emails))