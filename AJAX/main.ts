import express from 'express'
import expressSession from 'express-session'
import {Client} from 'pg'
import dotenv from 'dotenv'
import path from 'path'
import bodyParser from 'body-parser'
import multer from 'multer'
import { NodeMailgun } from 'ts-mailgun';
//import mailgunLoader from 'mailgun-js'
dotenv.config();

const app = express()

const mailer = new NodeMailgun();
mailer.apiKey = 'a78cced1dd15366f6eabc58d51e59502-3e51f8d2-d1cf3ea8';
mailer.domain = 'sandbox7239a73a49f047fa9bb2d3386c266e7c.mailgun.org';
mailer.fromEmail = 'noreply@sandbox7239a73a49f047fa9bb2d3386c266e7c.mailgun.org';
mailer.fromTitle = 'Book Reservation Notice';

// let mailgun = mailgunLoader({
//   apiKey: 'a78cced1dd15366f6eabc58d51e59502-3e51f8d2-d1cf3ea8',
//   domain: 'sandbox7239a73a49f047fa9bb2d3386c266e7c.mailgun.org'
//   domain: 'group9.ddns.net'
// });

// const sendEmail = (to: string, from: string, subject: string, content: string) =>{
//   let data = {
//     to,
//     from,
//     subject,
//     text: content
//   };
//   return mailgun.messages().send(data);
// }

const port = 8080

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })

const upload = multer({storage})

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname,'/public')));

//Session
app.use(expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave:true,
  saveUninitialized:true
}));

//SQL Connection
export const client = new Client({
    user:process.env.DB_USERNAME,     //project
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    port:5432
});

client.connect()

//mailer.init();

//Session Testing route
app.get('/session', (req,res)=>{
  if(req.session){
    console.log(req.session.counter)
    req.session.counter += 1;
  }
  res.end("Session res")
})

app.post('/reservation', upload.single('image'),async (req,res,next)=>{

  await client.query(`insert into reader_reservation (book_title, phone, email, name) VALUES ('${req.body.title}', '${req.body.contactnumber}', '${req.body.email}', '${req.body.customername}');`)

  const result = await client.query(`select * from sellers`)

  res.json(result.rows[0]) //可以一個route內insert幾多個table, 讀幾多個table都得，但response只可一個。
  next()
})

app.post('/email', upload.single('image'),async (req,res,next)=>{
  
});

//Get Book Recommend list to seller's report
app.get('/bookrecommendlist',async (req,res)=>{
  const result = await client.query(`select * from book_platform`)
  res.json(result.rows)
  return
})

//Delete Book Recommendation from seller's dashboard page
app.delete('/bookrecommend/:id', async (req,res)=>{    //:id係placeholder
  console.log(req.params.id)
  await client.query(`DELETE FROM book_platform WHERE id = ${req.params.id}`)
  res.json({Delete: 'Success'})
  return  
});

//較靚仔既死法
app.use(function(req,res){
  res.sendFile(path.join(__dirname,'public/404.html'));
})

app.listen(port,()=>{
    console.log("Listening at localhost:8080")
})