import axios from 'axios'        //由modules call axios
console.log(axios)

import fs from 'fs'           // 由modules call fs
console.log(fs)               // 話冇export, 搞唔掂  

import {lib} from './lib'           // lib係一個object
console.log(lib.someObject)         // Hello World  
console.log(lib.somefunction())     // expression is not callable

import {square} from './func'    // 由local folder搵, named export
console.log(square(5))           // 25  

import Cls from './Cls';         // default function from Cls.ts，係Cls.ts, default function係一個 num**3 既function
console.log(Cls(15))             // 3375

import {func} from './func'    
console.log(func(22))






