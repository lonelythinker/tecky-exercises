 function findFactors(num:number){
     let factors = [];
     for(let factor = 2; factor <= num / 2 ; factor++){
         if(num % factor === 0){
            factors.push(factor);
         }
     }
     return factors;
  }
  console.log(findFactors(12))

  function leapYear(year:number){
     if (year%400 == 0){
         console.log(`Year ${year} is a century leap year.`)
        
     } else if (year%4 == 0 && year%100 !== 0){
         console.log(`Year ${year} is a leap year.`)        
     } else {
         console.log(`Year ${year} is a ordinary year.`)
     }
 }

 let rnaResult: Array<String> = []
 let invalidInput : Array<String> = []
 let finalResult = ""                     /*="" 已夠資訊知道type係string*/
 function rnaTrans (dna: string){     
     for(const trans of dna){
       if(trans == "G"){
           rnaResult.push("C")
       } else if 
       (trans == "C"){
           rnaResult.push("G")
       } else if 
       (trans == "T"){
           rnaResult.push("A")
       } else if
       (trans == "A"){
           rnaResult.push("U")
       } else {
           rnaResult.push("?")
           invalidInput.push(trans)
       }
     }
     for(const rna of rnaResult){
         finalResult = finalResult + rna
     }
     return finalResult
 }

 console.log(rnaTrans("GCBTAGZT"))
 console.log(invalidInput)

 function factorial(number:number):number{
     if(number === 0 || number === 1){
        return 1;
     }
 
     return factorial(number - 1) * number
  }
 console.log(factorial(5))

type teacher = {
    name: string,
    age: number,
    students: student[]
}

type student = {
    name: string,
    age: number,
    exercises?: Exercise[]
}

type Exercise = {
    score: number,
    date: Date
}

const peter: teacher = {
    name: "Peter",
    age: 50,
    students:[
       { name:"Andy", age:20},
       { name:"Bob", age:23},
       {name: "Charlie", age:25 , exercises:[
           { score: 60 , date: new Date("2019-01-05") }
       ]}
    ]
}

const firstStudent = peter.students[2]

if(firstStudent.exercises){
    for(let exercise of firstStudent.exercises){
       console.log(exercise)
    }
}

const timeoutHandler = ():void=>{
    console.log("Timeout happens!");
/*return "123" 有return的話其實會error*/   

}

const timeout = 1000;

setTimeout(timeoutHandler,timeout);

const someValue : number | null = Math.random() > 0.5? 12: null;