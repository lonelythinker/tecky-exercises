type SpecialName = `Alice` | `Bob` | `Charlie`

let myName:SpecialName = "Charlie"

enum Direction {
    east = 1,
    south,
    west,
    north
}

function turnTo(direction:Direction){
    if(direction == Direction.north){
        console.log("This is the direction north!");
    } else {
        return "You entered wrong direction"
    }
    
}
console.log(turnTo(Direction.south))

function sum(nums:Array<number>):string{  
    let total = "";
    for(let num of nums){
       total = total + num;
    }
    return total;
}

console.log(sum([1,5,10,15,20]))