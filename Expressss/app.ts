import express from 'express'
//import expressSession from 'express-session'
import * as bodyParser from 'body-parser';
//import jsonfile from 'jsonfile'
import multer from 'multer'

const port = 8080
const app = express()

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `${__dirname}/public/uploads`); //真草圖片既path係度寫
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
  })
                     //將接收圖片既位置改做改名器既variable
                     //vvvvvvv 
const upload = multer({storage})

app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended:true}));  //處理body中的漢字
app.use(bodyParser.json());   //處理body中的json

app.post('/form',(req,res)=>{        //係html click link去「/form」, 就會trigger呢個response, 同click link去/form.html係兩回事
    console.log(req.body.username + req.query.username)     //係html form sumbit既data係對應<input name="xxx"> , submit既data就係"xxx"既value.  
    res.end("Form Data Received!!")      //name=username, 咁username就係obj key,submit既名就係key既value. 
})

app.get('/form', (req,res)=>{        //用method GET, 即使用form submit, submit既data都會顯示係URL
    console.log(req.query.nationality)
    res.header('content-type', 'text/plain; charset=utf8')  //text/plain唔打plain都work, 但text後個/一定要有
    res.end("已經收到你既國籍係" + req.query.nationality!!)
})

app.post('/uploadphoto',upload.single('photo'), (req,res)=>{
    console.log(req.file)
    res.header('content-type', 'text/plain; charset=utf8')
    res.end("已經收到你既圖片叫" + req.file.originalname)
})

//         //vvvv path
// app.get('/name/:fruit',function(req,res){    
//               //^^^^ key, client在URL輸入的東西為value
//     console.log(req.params.fruit)
//     res.end(req.params.fruit)
//     return
// })

// app.post('/name',(req,res)=>{
//     const {name,location} = req.body;
//     res.end(`Name is ${name}, Location is ${location}`);
//     console.log(req.body)
// });



app.listen(port,function(){
    console.log(`Listening at local host ${port} port`)
})