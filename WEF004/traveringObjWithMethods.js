const array = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]

const object = array[0]
const keysInArray = Object.keys(object)
const currencyKeys = Object.keys(object.currencies[0])
const langKeys = Object.keys(object.languages[0])
const transKeys = Object.keys(object.translations)

keysInArray.map(function(eachKey){
    if(eachKey!=='currencies' && eachKey!=='languages' && eachKey!=='translations'){
        console.log(`${eachKey[0].toUpperCase()}${eachKey.slice(1)}` + ": " + `${object[eachKey]}`)

    } else if(eachKey=='currencies'){
        currencyKeys.map(function(eachCurrKey){
            console.log("Currencies_"+ eachCurrKey[0].toUpperCase() + eachCurrKey.slice(1) + ": " + object.currencies[0][eachCurrKey])
        })
            
    } else if(eachKey=='languages'){
        langKeys.map(function(eachLangKey){
            console.log("Languages_" + eachLangKey[0].toUpperCase() + eachLangKey.slice(1) + ": " + object.languages[0][eachLangKey])
            console.log("Languages_" + eachLangKey[0].toUpperCase() + eachLangKey.slice(1) + ": " + object.languages[1][eachLangKey])
        })
    } else if(eachKey=='translations'){
        transKeys.map(function(eachTransKey){
            console.log("Translations_" + eachTransKey[0].toUpperCase()+ eachTransKey.slice(1) + ": " + object.translations[eachTransKey])
        })        
    }
})

/*
Name: Hong Kong
TopLevelDomain: .hk
Alpha2Code: HK
Alpha3Code: HKG
CallingCodes: 852
Capital: City of Victoria
AltSpellings: HK,香港
Region: Asia
Subregion: Eastern Asia
Population: 7324300
Latlng: 22.25,114.16666666
Demonym: Chinese
Area: 1104
Gini: 53.3
Timezones: UTC+08:00
Borders: CHN
NativeName: 香港
NumericCode: 344
Currencies_Code: HKD
Currencies_Name: Hong Kong dollar
Currencies_Symbol: $
Languages_Iso639_1: en
Languages_Iso639_1: zh
Languages_Iso639_2: eng
Languages_Iso639_2: zho
Languages_Name: English
Languages_Name: Chinese
Languages_NativeName: English
Languages_NativeName: 中文 (Zhōngwén)
Translations_De: Hong Kong
Translations_Es: Hong Kong
Translations_Fr: Hong Kong
Translations_Ja: 香港
Translations_It: Hong Kong
Translations_Br: Hong Kong
Translations_Pt: Hong Kong
Translations_Nl: Hongkong
Translations_Hr: Hong Kong
Translations_Fa: هنگ‌کنگ
Flag: https://restcountries.eu/data/hkg.svg
RegionalBlocs:
Cioc: HKG
*/