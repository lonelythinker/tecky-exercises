const array = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]

const object = array[0]
const keysInArray = Object.keys(object)

    for(const eachKey of keysInArray){
        if(eachKey!=='currencies' && eachKey!=='languages' && eachKey!=='translations'){
          console.log(`${eachKey[0].toUpperCase()}${eachKey.slice(1)}: ` + `${object[eachKey]}`)  
        } else if(eachKey=='currencies'){
            for(const subkey01 in object.currencies[0]){
                console.log("Currencies_" + subkey01[0].toUpperCase() + subkey01.slice(1) + ": " + object.currencies[0][subkey01])
            } 
        } else if(eachKey=='languages'){
            for(const subkey02 in object.languages[0]){
                console.log("Languages_" + subkey02[0].toUpperCase() + subkey02.slice(1) + ": " + object.languages[0][subkey02])
            }
            for(const subkey03 in object.languages[1]){
                console.log("Languages_" + subkey03[0].toUpperCase() + subkey03.slice(1) + ": " + object.languages[1][subkey03])
            }
        } else if(eachKey=='translations'){
            for(const subkey04 in object.translations){
                console.log("Translations_" + subkey04[0].toUpperCase() + subkey04.slice(1) + ": " + object.translations[subkey04])
            }
        }
    }

/*
Name: Hong Kong
TopLevelDomain: .hk
Alpha2Code: HK
Alpha3Code: HKG
CallingCodes: 852
Capital: City of Victoria
AltSpellings: HK,香港
Region: Asia
Subregion: Eastern Asia
Population: 7324300
Latlng: 22.25,114.16666666
Demonym: Chinese
Area: 1104
Gini: 53.3
Timezones: UTC+08:00
Borders: CHN
NativeName: 香港
NumericCode: 344
Currencies_Code: HKD
Currencies_Name: Hong Kong dollar
Currencies_Symbol: $
Languages_Iso639_1: en
Languages_Iso639_2: eng
Languages_Name: English
Languages_NativeName: English
Languages_Iso639_1: zh
Languages_Iso639_2: zho
Languages_Name: Chinese
Languages_NativeName: 中文 (Zhōngwén)
Translations_De: Hong Kong
Translations_Es: Hong Kong
Translations_Fr: Hong Kong
Translations_Ja: 香港
Translations_It: Hong Kong
Translations_Br: Hong Kong
Translations_Pt: Hong Kong
Translations_Nl: Hongkong
Translations_Hr: Hong Kong
Translations_Fa: هنگ‌کنگ
Flag: https://restcountries.eu/data/hkg.svg
RegionalBlocs:
Cioc: HKG
*/