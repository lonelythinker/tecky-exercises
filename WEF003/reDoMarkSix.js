// Exercise 01
const markSixResult = [1, 3, 5, 7, 9, 11]

function checkMarkSix(markSixResult,bid) {
    let count = 0
    console.log("The Mark6 Result is: " + "["+markSixResult+"]")
    console.log("Your Quick Pick is: " + "["+bid+"]")
    for(const num of bid){
        if(markSixResult.includes(num)){          //可以唔打===true.
        count += 1
        }
    }
    if(count===2){                                //如果中兩個字，即kill下面line 37的setInterval
        clearInterval(repeater)
    }
    return count ===2 ? "WIN":"LOSE"              //return得,即係"count===2"成立，本身boolean值係true, 否則就係false.
}                                                 //再配合Ternary operator使用

// Exercise 02

function quickPick(){
    let picks = [];
    let pick01 = Math.floor(Math.random()*49+1);
    picks.push(pick01)
    let pick02 = Math.floor(Math.random()*49+1);

    while(pick02===pick01){                           // 用while不用if, 用while就會一直執行line 25 直到 line 24 變false先執行line 27
        pick02 = Math.floor(Math.random()*49+1);      // 嘗試唔用method既情況下解決重複號碼問題
    }        
        picks.push(pick02)
        
    return picks    
}

//console.log(checkMarkSix(markSixResult, quickPick()))     //逐次執行是正常的

let repeater = setInterval(function(){
     console.log(checkMarkSix(markSixResult, quickPick()))  //但加上setInterval後就會出錯，明明中左都出LOSE, 冇中又出WIN.
},200);                                                     //已找到原因，之前將let count寫左係function外， 一round中一個pick就累積左1,另一round中一個又加1, function以為win左.  
                                                            //將let count=0寫返入function每次reset=0就ok.   
/*
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [7,32]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [4,22]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [25,10]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [26,12]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [20,39]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [2,32]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [26,47]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [43,26]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [4,49]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [2,47]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [45,27]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [29,15]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [22,42]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [18,37]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [10,11]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [16,11]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [3,16]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [37,7]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [43,14]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [13,12]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [33,9]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [10,43]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [17,28]
LOSE
The Mark6 Result is: [1,3,5,7,9,11]
Your Quick Pick is: [1,11]
WIN                                         //一WIN就停
*/