let rnaResult = []
let invalidInput = []
let finalResult = ""
function rnaTrans (dna){
    for(const trans of dna){
      if(trans == "G"){
          rnaResult.push("C")
      } else if 
      (trans == "C"){
          rnaResult.push("G")
      } else if 
      (trans == "T"){
          rnaResult.push("A")
      } else if
      (trans == "A"){
          rnaResult.push("U")
      } else {
          rnaResult.push("?")
          invalidInput.push(trans)
      }
    }
    for(i=0;i<rnaResult.length;i++){
        finalResult = finalResult + rnaResult[i]
    }
    return finalResult
}

console.log(rnaTrans("GCBTAGZT"))
console.log(invalidInput)