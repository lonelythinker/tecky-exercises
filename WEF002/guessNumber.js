//const readLineSync = require('readLine-Sync')
const computerNum = Math.floor(Math.random()*11)

let attempt = 3

while(attempt > 0){
let playerNum = prompt(`You will have ${attempt} chances to guess a number between 0 to 10, please pick your guess: `)

    if(isNaN(playerNum) || playerNum>10 || playerNum<0){  //if user input is NaN(not a 0-9 number), true, keep executing line 10-11. if not NaN, go line 14
        console.log("You have to enter a number between 0 to 10 !")
        continue;        // Make sure user key in number 0-9, before really go to the game
    
    } if (playerNum == computerNum){
        console.log("Congrats, you win!")
        break;
    } else if (playerNum > computerNum){
        console.log("Try a smaller one!")
        attempt--
    } else if (playerNum < computerNum){
        console.log("Try a larger one!")
        attempt--
    }
    
    console.log("You still have " + attempt + " more chances.")
    if (attempt === 0){
        console.log("The answer is " + computerNum + " !" + " You're out of chance.")
    }
}