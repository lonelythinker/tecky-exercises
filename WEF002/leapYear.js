function leapYear(year){
    if (year%400 == 0){
        console.log(`Year ${year} is a century leap year.`)
    } else if (year%4 == 0 && year%100 !== 0){
        console.log(`Year ${year} is a leap year.`)        
    } else {
        console.log(`Year ${year} is a ordinary year.`)
    }
}
leapYear(1600)
leapYear(1996)
leapYear(1991)
leapYear(1800)