console.log(true)
console.log(!22)
console.log(0)

console.log(!true)
console.log(!!1)
console.log(!"")
console.log([!!""])  //用兩個!!只係反映出intrinsic value

console.log("Let's go")
console.log('Let\'s go') //用escape character

console.log("\\")  //用兩個backslash以顯示一個backslash

console.log(`
Happy Birthday to you~

Happy Birthday to you~
Happy Birthday to you~
`)

console.log(`Let's go "Wonderful World`)
console.log(`"Let's go "Wonderful World"`)
console.log(`\`Let's go "Wonderful World\``)
console.log("2" === 2) //false

