import express, { NextFunction } from 'express'
import {Request,Response} from 'express'
//import next from 'express'
import path from 'path'
//import { runInNewContext } from 'vm';
import expressSession from 'express-session'
//one express app holds one port
const app = express()
const port = 8080

//Add this line
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

// app.get('/',function(req:Request,res:Response){
//     res.end("Hello World!!")
// });

// app.get('/', function(req,res){
    
//     res.end("I don't know what I'm doing")
//     console.log(req.query)
// })

app.get('/name/:name/loc/:location/sex/:sex', function(req,res){
    const {name, location, sex} = req.params
    res.end(`Name is ${name}, Location is ${location}, Sex is ${sex}`)
    console.log(req.params)
})

app.get('/abcdef',function(req:Request,res:Response){       //直接寫回信
    res.end("Hello World from abcdef!!")
});

app.get('/website',function(req:Request,res:Response){      //如果client去website呢度，就serve下面個html比佢
    res.sendFile(path.join(__dirname,'public/index.html'))  //「__dirname」指依家呢個directory, pwd果個, must absolute path
})                                                          //比另一樣野做回信

app.use(express.static('public'))                           //有親咩request, 走入public個folder睇睇有冇先。有呢一句就搞掂。

app.get('/website',function(req:Request,res:Response,next:NextFunction){

    console.log("I'm 截胡function")
    next() 

    res.sendFile(path.join(__dirname,'public/index.html'))
});

//行埋下面呢句先開始霸住個port
app.listen(port,function(){
    console.log(`Listening at local host ${port} port`)
})