import express from 'express';
import expressSession from 'express-session'
import path from 'path'

//server side code不應被user access
const app = express ()

// Add this line          what is this actually??
app.use(expressSession({
    secret: 'Tecky Academy teaches typescript',
    resave:true,
    saveUninitialized:true
}));

app.use(function(req,res,next){
    
    if(req.session){
        if(req.session.counter){
            req.session.counter += 1;    
        }else{
            req.session.counter = 1
        }
      console.log(`Counter is ${req.session.counter}`);  
    }
    next()      //一係自己答，一係next比下一個middleware, 唔可以又唔答又唔next, 唔係就loop死
})

app.use(function(req,res,next){
    console.log(`[${new Date().toISOString()}] Request ${req.path}`);   //req.path會比返依家個request攞緊d咩
    next()
})

app.use(express.static('public'));   //乜path都唔寫即係大包圍

app.use(function(req,res){          //最後，pagenotfound handle
    res.sendFile(path.join(__dirname,'public/404.html'))
})

app.listen(8080, function(){
    console.log("Express Listening at http://localhost:8080")
})
